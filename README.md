# SugarCUB (en / fr)

[![Code Climate](https://codeclimate.com/github/bronycub/sugarcub/badges/gpa.svg)](https://codeclimate.com/github/bronycub/sugarcub)
[![Gitlab CI](https://ci.gitlab.com/projects/775/status.png?ref=dev)](https://ci.gitlab.com/projects/775/status.png?ref=dev)

SugarCUB is a customizable web site with the purpose to allow any one to easily create their site for their brony collective. It's just a matter of installing it and customizing it to your liking. And it's a free software so if you find any bug or want to improve it, just come share your thoughts with us /). Even better if your a programmer, help is always wanted !

SugarCUB est un site web configurable avec l'objectif de permettre à n'importe qui de créer son propre site pour son collectif brony. C'est aussi simple que de l'installer et de le configuer selon vos gouts. C'est aussi un logicel libre donc si vous trouvez un bug ou que vous souhaitez l'améliorer, venez simplement nous en parler /). Encore mieux si vous êtes un programmeur, l'aide est toujours la bienvenue !

# Installation

TODO when at least a test release is ready.

# Report a bug or submit wishes / Signaler un bug ou soumettre une idée

To report a bug or to submit us an idea, just go to the "Issues" page and create a new issue with the tag bug or feature-request accordingly (you will need to be connected on gitlab).

Pour signaler un bug ou soumettre une idée, allez simplement sur la page "Issues" et créez une issue avec le tag bug ou feature-request (vous aurez besoin d'être connecté sur gitlab).

# Contribute

TODO

# License / Licence

This project is released under GPL3. See LICENSE.txt for more information or visit http://www.gnu.org/licenses/.

Ce projet est distribué sous licence GPL3. Voir LICENSE.txt pour plus d'informations ou visitez  http://www.gnu.org/licenses/.

